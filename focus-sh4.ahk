;===============================================================================
#NoEnv	; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn	; Enable warnings to assist with detecting common errors.
#SingleInstance force
#InstallMouseHook
#InstallKeybdHook
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
SendMode Event
;===============================================================================


; Note: All Windows OSes should be able to play .wav files. However, other files
; (.mp3, .avi, etc.) might not be playable if the right codecs or features 
; aren't installed on the OS.
;
; To bypass this potential issue We are now using .wav files instead of .mp3.
;
; We are now using lowered sound files (gain modified -18 dB).

;-- BEGIN: CONFIGURATION -------------------------------------------------------
; FS_ = This is the module prefix for Focus-SH4 script; all variables are 
; prefixed with it.

; FS_DBG = Debugging	
;
; 1 for YES, 0 for NO
; Currently, all this does is write to .txt log file(s) in the .AHK directory
FS_DBG:=1
;
if (FS_DBG = 1) {
	FileAppend, ---new run begins here---`n, log.fs.txt
	FileAppend, [FS] Started reading configuration`n, log.fs.txt
	FileAppend, [FS] Debugging is enabled`n, log.fs.txt
}

; FS_FE = Focus Effort (level)
;
; 0 NO EFFORT		i.e.: turn automatic SH4 window activation off
;
; 1 SLIGHT EFFORT	default value = 5000 milliseconds = 5 seconds
; This is good for example use case:
;"I'll write an email/forums-post now but switch to sh4 later!"
;
; 2 FOCUSED EFFORT	default value = 250 milliseconds = 0.25 seconds
; "I'm actively playing the game, keep the radio/gramophone/crew sounds on!"
;
FS_FE:=2
;
if (FS_DBG = 1) {
	FileAppend, [FS] FS_FE initial value has been read as: %FS_FE%`n, log.fs.txt
}

; FS_HK = The physical hotkey used to toggle Focus-SH4 script on/off
;
; Note: this is only for logging purposes and key is actually set below
; changing this will NOT change the hotkey
;
FS_HK := "SHIFT+F2"
;
if (FS_DBG = 1) {
	FileAppend, [FS] FS_HK value has been read as: %FS_HK%`n, log.fs.txt
}

; FS_FE1 & 2 = Idle Time (in milliseconds) for Focus Effort levels 1 and 2
FS_FE1:=5000
FS_FE2:=250
;
if (FS_DBG = 1) {
	FileAppend, [FS] FS_FE1 value has been read as: %FS_FE1%`n, log.fs.txt
	FileAppend, [FS] FS_FE2 value has been read as: %FS_FE2%`n, log.fs.txt
}

; FS_IT = Idle Time (in milliseconds)
; This is driven by the Focus Effort (level) system above
;
; Set this to the value that is used for initial Focus Effort level
FS_IT:=FS_FE2
;
if (FS_DBG = 1) {
	FileAppend, [FS] FS_IT value has been read as: %FS_IT%`n, log.fs.txt
}
;
if (FS_DBG = 1) {
	FileAppend, [FS] Completed reading configuration`n, log.fs.txt
}
;-- END: CONFIGURATION ---------------------------------------------------------



;-------------------------------------------------------------------------------
; play initial 'Current Level Announcement' sound
If (FS_FE=0) {
	SoundPlay, 0.minus_18_dB.wav
	if (FS_DBG = 1) {
		FileAppend, [FS] Played 0.minus_18_dB.wav as FS_FE value is: %FS_FE%`n, log.fs.txt
	}
} Else If (FS_FE=1) {
	SoundPlay, 1.minus_18_dB.wav
	if (FS_DBG = 1) {
		FileAppend, [FS] Played 1.minus_18_dB.wav as FS_FE value is: %FS_FE%`n, log.fs.txt
	}
} Else If (FS_FE=2) {
	SoundPlay, 2.minus_18_dB.wav
	if (FS_DBG = 1) {
		FileAppend, [FS] Played 2.minus_18_dB.wav as FS_FE value is: %FS_FE%`n, log.fs.txt
	}
}
;-------------------------------------------------------------------------------
Loop {
	Sleep, 50
	If (FS_FE > 0 And A_TimeIdle > FS_IT) {
		If WinExist("SilentHunter4")
		    WinActivate ; Use the window found by WinExist.
	}
}
;-------------------------------------------------------------------------------
+F2::
If (FS_DBG = 1) {
	FileAppend, [FS] Hot key instruction FS_HK (%FS_HK%) keypress detected`n, log.fs.txt
}
; Modify Focus Effort (level) value at runtime.
; It only goes up. There are 3 settings, thus the possible transitions are:
; 	0 > 1
; 	1 > 2
;	2 > 0
;
If (FS_FE=0) {
	FS_FE:=1
	if (FS_DBG = 1) {
		FileAppend, [FS] FS_FE value is now: %FS_FE%`n, log.fs.txt
	}
	SoundPlay, 1.minus_18_dB.wav
	if (FS_DBG = 1) {
		FileAppend, [FS] Played 1.minus_18_dB.wav as FS_FE value is: %FS_FE%`n, log.fs.txt
	}
	FS_IT:=FS_FE1
} Else If (FS_FE=1) {
	FS_FE:=2
	if (FS_DBG = 1) {
		FileAppend, [FS] FS_FE value is now: %FS_FE%`n, log.fs.txt
	}
	SoundPlay, 2.minus_18_dB.wav
	if (FS_DBG = 1) {
		FileAppend, [FS] Played 2.minus_18_dB.wav as FS_FE value is: %FS_FE%`n, log.fs.txt
	}
	FS_IT:=FS_FE2
	if (FS_DBG = 1) {
		FileAppend, [FS] FS_FE value is now: %FS_FE%`n, log.fs.txt
	}
} Else {
	FS_FE:=0
	if (FS_DBG = 1) {
		FileAppend, [FS] FS_FE value is now: %FS_FE%`n, log.fs.txt
	}
	SoundPlay, 0.minus_18_dB.wav
	if (FS_DBG = 1) {
		FileAppend, [FS] Played 0.minus_18_dB.wav as FS_FE value is: %FS_FE%`n, log.fs.txt
	}
	FS_IT:=-1
}
Return
